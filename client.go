package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"net/http"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 8096
)

var (
	newline       = []byte{'\n'}
	space         = []byte{' '}
	lastId  int64 = 0
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type HeadPos struct {
	X int `json:"x"`
	Y int `json:"y"`
	Z int `json:"z"`
}

type HeadRot struct {
	X       float64 `json:"x"`
	Y       float64 `json:"y"`
	Z       float64 `json:"z"`
	Updated bool    `json:"-"`
}

type HeadInfo struct {
	Pos      HeadPos `json:"pos"`
	Rot      HeadRot `json:"rot"`
	Nickname string  `json:"nickname"`
}

type RotUpdateMessage struct {
	HeadRots map[int64]HeadRot `json:"rots"`
}

type OnJoinMessage struct {
	Id         int64              `json:"joinId"`
	AllPlayers map[int64]HeadInfo `json:"allPlayers"`
	HeadInfo   HeadInfo           `json:"headInfo"`
}

type AddPlayerMessage struct {
	Id       int64    `json:"addId"`
	HeadInfo HeadInfo `json:"headInfo"`
}

type RemovePlayerMessage struct {
	Id int64 `json:"removeId"`
}

type IncomingMessage struct {
	Cmd string  `json:"cmd"`
	Rot HeadRot `json:"rot"`
}

type Client struct {
	hub *Hub

	conn *websocket.Conn

	send chan []byte

	id int64

	headInfo HeadInfo
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.broadcastRemovePlayer(c.id)
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })

	var im IncomingMessage

	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				//log.Printf("error: %v", err)
			}
			break
		}

		jsonErr := json.Unmarshal(message, &im)
		if jsonErr == nil {
			if im.Cmd == "upd" {
				if c.headInfo.Rot.X != im.Rot.X || c.headInfo.Rot.Y != im.Rot.Y || c.headInfo.Rot.Z != im.Rot.Z {
					c.headInfo.Rot = im.Rot
					c.headInfo.Rot.Updated = true
				} else {
					c.headInfo.Rot.Updated = false
				}
			} else {
				log.Printf("unknown command %s", im.Cmd)
			}
		}
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func (c *Client) onJoin() {

	ojm := OnJoinMessage{
		Id:         c.id,
		AllPlayers: c.hub.GetAllPlayers(),
		HeadInfo:   c.headInfo,
	}

	message, jsonEnErr := json.Marshal(ojm)
	if jsonEnErr == nil {
		c.send <- message
	}

	//log.Printf("OnJoin #%d\n", c.id)
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func SpawnHead(id int64) HeadInfo {
	x := randInt(-120, 120)
	y := randInt(50, 70)
	z := randInt(290, 310)
	pos := HeadPos{x, y, z}
	rot := HeadRot{0, 0, 0, false}

	log.Printf("Spawned head #%d at x=%d\n", id, x)
	headInfo := HeadInfo{
		Pos:      pos,
		Rot:      rot,
		Nickname: fmt.Sprintf("id%d", id),
	}
	return headInfo
}

// serveWs handles websocket requests from the peer.
func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	lastId++
	headInfo := SpawnHead(lastId)
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256), id: lastId, headInfo: headInfo}
	client.hub.register <- client
	go client.writePump()
	client.onJoin()
	hub.broadcastAddPlayer(client)
	client.readPump()
}
