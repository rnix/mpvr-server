package main

import (
	"encoding/json"
	//"log"
	"time"
)

const (
	updateTickPeriod = time.Second / 60
)

type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

func newHub() *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func (h *Hub) GetAllPlayers() (allPlayers map[int64]HeadInfo) {
	allPlayers = make(map[int64]HeadInfo)

	for client, _ := range h.clients {
		allPlayers[client.id] = client.headInfo
	}
	return
}

func (h *Hub) GetRotUpdateMessage() (rotUpdateMessage RotUpdateMessage) {
	rotUpdateMessage.HeadRots = make(map[int64]HeadRot)

	for client, _ := range h.clients {
		if client.headInfo.Rot.Updated {
			rotUpdateMessage.HeadRots[client.id] = client.headInfo.Rot
		}
	}

	return
}

func (h *Hub) broadcastAddPlayer(c *Client) {
	//log.Printf("Broadcast add player #%d\n", c.id)
	m := AddPlayerMessage{
		Id:       c.id,
		HeadInfo: c.headInfo,
	}

	message, jsonEnErr := json.Marshal(m)
	if jsonEnErr == nil {
		h.broadcast <- message
	}
}

func (h *Hub) broadcastRemovePlayer(id int64) {
	//log.Printf("Broadcast remove player #%d\n", id)
	m := RemovePlayerMessage{
		Id: id,
	}

	message, jsonEnErr := json.Marshal(m)
	if jsonEnErr == nil {
		h.broadcast <- message
	}
}

func (h *Hub) updateTicks() {
	ticker := time.NewTicker(updateTickPeriod)
	defer func() {
		ticker.Stop()
	}()
	for {
		select {
		case <-ticker.C:

			rotUpdateMessage := h.GetRotUpdateMessage()

			encodedMessage, jsonEnErr := json.Marshal(rotUpdateMessage)
			if jsonEnErr == nil && len(rotUpdateMessage.HeadRots) > 0 {
				h.broadcast <- encodedMessage
			}
		}
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
